import re
from pathlib import Path
from pprint import pprint

#TODO REMOVE THOSE LIBRARY
import time

class awsprofiles:

    def __init__(self):
        #TODO prompt for the location if it's not stored here and store it in a config file
        self.awsConfig = Path.home() / ".aws/config"
        self.profileRegex = re.compile(r'\[(.*)\]')
        self.profileConfig = []

    def getProfiles(self):
        if not self.awsConfig.exists():
            return null
    
        awsConfigReader = open(self.awsConfig, 'r')
        for line in awsConfigReader.readlines():
            if self.profileRegex.search(line) is not None:
                    formatted_profile_name = line[9:len(line)-2] if "profile" in line else line[1:len(line)-2]
                    self.profileConfig.append(formatted_profile_name)

        return self.profileConfig