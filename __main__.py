import os, curses, subprocess, boto3
from botocore.config import Config
from awsprofiles import awsprofiles

import logging

ENTER = 10

class Display:
    def __init__(self,screen):
        self.cursor_x = 0
        self.cursor_y = 0
        self.height = 0
        self.width = 0
        self.active_profile = ""
        self.selected_entry = ""
        self.session = ""

        #lists
        self.profiles = awsprofiles().getProfiles()
        self.curses_entries = self.profiles.copy()

        self.draw_menu(screen)

    def move(self, key):
        if key == curses.KEY_DOWN:
            self.cursor_y = self.cursor_y + 1
        elif key == curses.KEY_UP:
            self.cursor_y = self.cursor_y - 1
        elif key == curses.KEY_RIGHT:
            self.cursor_x = self.cursor_x + 1
        elif key == curses.KEY_LEFT:
            self.cursor_x = self.cursor_x - 1

        self.cursor_x = max(0, self.cursor_x)
        self.cursor_x = min (self.width-1, self.cursor_x)

        self.cursor_y = max(0, self.cursor_y)
        self.cursor_y = min (self.height-1, self.cursor_y)

        self.selected_entry = self.curses_entries[self.cursor_y]
        self.logger.info(self.selected_entry)


    """
    We're splitting the active curses_entries list where the cursor position is
    so we can add the ec2 instances below the profile, and then re-add the following
    profiles below the instances.
    """
    def edit_menu_entries(self, ec2_instances):
        if(self.menu_is_folded(ec2_instances['Reservations'][0]['Instances'])):
            tmp_list = self.curses_entries[0:self.cursor_y+1].copy()
            for i in range(len(ec2_instances['Reservations'])):
                tmp_list.append(self.get_name_tag(ec2_instances['Reservations'][i]['Instances'][0]['Tags']) + " (" + ec2_instances['Reservations'][i]['Instances'][0]['InstanceId'] + ")")
                #stdscr.addstr(height, 0, session.getEnv() +" : " + session.getName())
            for j in range(self.cursor_y+1, len(self.curses_entries)):
                tmp_list.append(self.curses_entries[j])
            
            self.curses_entries = tmp_list.copy()
        else:
            self.curses_entries = self.profiles.copy()
    
    """
    The name displayed in the aws console is equal to the value of 
    the tag with "Name" as its key.
    """
    def get_name_tag(self, Tags):
        for tag in Tags:
            if(tag['Key'] == "Name"):
                return tag['Value']
                #print(list_item['Value'])
                #stdscr.addstr(start_y + i, start_x_keystr, list_item['Value'])
        return "noname"

    """
    Determine if the curses menu is folded (ie. not instances are displayed).
    It's used to determine if the profile entry should be folded or unfolded (display all instances).
    #TODO Find a better way to do this, as if two profiles are unfolded, they're both folded following the result of this method
    """
    def menu_is_folded(self, instances):
        for instance in instances:
            for entry in self.curses_entries:
                if(instance['InstanceId'] in entry):
                    return False
        return True
    
    def get_instance_id(self, ec2_instances):
        for instance_no in range(len(ec2_instances['Reservations'])):
            self.logger.info(ec2_instances['Reservations'][instance_no]['Instances'][0]['InstanceId'])
            self.logger.info(self.selected_entry)
            if(ec2_instances['Reservations'][instance_no]['Instances'][0]['InstanceId'] in self.selected_entry):
                return ec2_instances['Reservations'][instance_no]['Instances'][0]['InstanceId']

    def new_session(self):
        env = os.environ.copy()
        env['AWS_PROFILE'] = self.active_profile
        self.logger.info("Creating session ... active profile: " + self.active_profile)
        try:
            self.session = boto3.session.Session(profile_name=self.active_profile)
            ec2_instances = self.session.client('ec2').describe_instances()
            self.edit_menu_entries(ec2_instances)
        except Exception() as e:
            print(e)
    
    def launch_ssm_prompt(self):
        ec2_instances = self.session.client('ec2').describe_instances()
        cmd = ['aws', 'ssm', 'start-session', '--target', str(self.get_instance_id(ec2_instances))]
        env = os.environ.copy()
        env['AWS_PROFILE'] = str(self.active_profile)

        p = subprocess.Popen(cmd, env=env)
        p.wait()

    def draw_menu(self, stdscr):
        #TODO remove
        self.logger = logging.getLogger(__file__)
        hdlr = logging.FileHandler(__file__ + ".log")
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        self.logger.addHandler(hdlr)
        self.logger.setLevel(logging.DEBUG)
        
        key = 0
        self.cursor_x = 0
        self.cursor_y = 0

        # Clear and refresh the screen for a blank canvas
        stdscr.erase()
        stdscr.refresh()

        # Start colors in curses
        curses.start_color()
        curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)

        # Loop where k is the last character pressed
        while (key != ord('q')):

            # Initialization
            stdscr.clear()
            self.height, self.width = stdscr.getmaxyx()

            if key in [curses.KEY_DOWN, curses.KEY_UP, curses.KEY_RIGHT, curses.KEY_LEFT]:
                self.move(key)

            if key == ENTER:
                if(self.selected_entry in self.profiles): #if the selected entry is a profile (and not an instance)
                    self.active_profile = self.selected_entry
                    self.new_session()

                else: #it's an ec2 instance
                    #ssm = boto3.client('ssm')
                    #ssm = self.session.client('ssm')
                    #ssm.start_session(Target = self.get_instance_id(ec2_instances))
                    self.launch_ssm_prompt()

            # display
            statusbarstr = "Press 'enter' on a profile to see attached instances and 'enter' on an instance to start the ssm shell, or 'q' to exit | Pos: {}, {}".format(self.cursor_x, self.cursor_y)
            
            height = 0
            for entry in self.curses_entries:
                style = curses.A_STANDOUT if height == self.cursor_y else curses.A_NORMAL
                width = 0 if entry in self.profiles else 5
                stdscr.addstr(height, width, entry, style)
                height += 1

            # Render status bar
            stdscr.attron(curses.color_pair(3))
            stdscr.addstr(self.height-1, 0, statusbarstr)
            stdscr.addstr(self.height-1, len(statusbarstr), " " *  (self.width - len(statusbarstr) - 1))
            stdscr.attroff(curses.color_pair(3))

            # Refresh the screen
            stdscr.refresh()

            # Wait for next input
            key = stdscr.getch()

def main():
    curses.wrapper(Display)

if __name__ == "__main__":
    main()