try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name = 'eec2',
    keywords="aws, ec2, ssm, cli",
    author="Philippe Ouellette",
    description="Easy ec2 access uses configured aws cli profiles to connect to ec2 instances using ssm",
    url="https://gitlab.com/mediagrif/...",
    download_url="https://gitlab.com/mediagrif/...",
    author_email="philippe.ouellette@mdfcommerce.com",
    version="1.0.0",
    install_requires=["boto3"],
    packages=["eec2"],
    entry_points={"console_scripts": ['eec2 = __main__:main']},
    classifiers=["Programming Language :: Python :: 3.7",]
)